import json
from vibrate_clicker import linksToArrays

if __name__ == "__main__":
    with open('kdo_ne_obstaja.json','r') as fh:
        ary=json.loads(fh.read())

    already_added = []
    not_compatible = []
    to_be_added = []

    last_band_id = ary[len(ary)-1]["id"]

    for band in ary:
        if last_band_id == band["id"]:
            to_be_added.append(band)
            print("last band: "+str(last_band_id))
            continue
        _, _, _, no1, no2 = linksToArrays(band["links"])
        if "added" not in band.keys():
            if (no1 > 0) and (no2 > 0):
                already_added.append(band)
            else:
                not_compatible.append(band)
            continue
        elif band["added"] == 1 and (no1 > 0) and (no2 > 0):
            already_added.append(band)
            continue
        elif (no1 > 0) and (no2 > 0):
            to_be_added.append(band)
            continue
        else:
            not_compatible.append(band)
            continue

    print("added: "+str(len(already_added)) + " not compatible: "+str(len(not_compatible))+" to be added: "+str(len(to_be_added)))
    with open('kdo_ne_obstaja.json', 'w') as fh:
        fh.write(json.dumps(to_be_added, indent=4))
