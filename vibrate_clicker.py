from pynput.keyboard import Controller as kbC
from pynput.keyboard import Key
from pynput.mouse import Button, Controller
import time
import json
from PIL import ImageChops, Image, ImageGrab

# Če prestavš ekran spremen tole
relative_x = 0
relative_y = 0

mouse = Controller()
keyboard = kbC()


def mouseMove(x, y):
    mouse.position = (x + relative_x, y + relative_y)


def mouseClick():
    mouse.press(Button.left)
    mouse.release(Button.left)


def mouseClickWait():
    mouseClick()
    time.sleep(0.5)


def mouseMoveClickWait(coor):
    mouseMove(coor[0], coor[1])
    mouseClickWait()


def keyboardClick(key):
    keyboard.press(key)
    keyboard.release(key)


CursorPositions = {
    "+ADD": (1785, 26),
    "Artist": (1794, 63),
    "ArtistName": (928, 503)
}


def linksToArrays(links):
    # 1st array orderd
    frstPageMust = {}
    fpm = 0
    frstPage = {}
    fp = 0
    if (links["spotify"] != ""):
        frstPageMust["spotify"] = (links["spotify"])
        fpm += 1

    if (links["facebook"] != ""):
        frstPageMust["facebook"] = (links["facebook"])
        fpm += 1

    if (links["twitter"] != ""):
        frstPage["twitter"] = (links["twitter"])
        fp += 1

    if (links["soundcloud"] != ""):
        frstPage["soundcloud"] = (links["soundcloud"])
        fp += 1

    if (links["youtube"] != ""):
        frstPage["youtube"] = (links["youtube"])
        fp += 1

    if (links["instagram"] != ""):
        frstPage["instagram"] = (links["instagram"])
        fp += 1

    # second page
    secPage = {}
    sp = 0

    if (links["official website"] != ""):
        secPage["homepage"] = (links["official website"])
        sp += 1
    elif (links["homepage"] != ""):
        secPage["homepage"] = (links["homepage"])
        sp += 1
    elif (links["bandcamp"] != ""):
        secPage["homepage"] = (links["bandcamp"])
        sp += 1
    elif (links["myspace"] != ""):
        secPage["homepage"] = (links["myspace"])
        sp += 1

    if (links["google+"] != ""):
        secPage["google+"] = (links["google+"])
        sp += 1

    if (links["itunes"] != ""):
        secPage["itunes"] = (links["itunes"])
        sp += 1

    return frstPageMust, frstPage, secPage, fpm, (sp + fp)


def analize_histograms(a):
    nulls = 0
    non_nulls = 0
    all = 0
    for n in a:
        if (n == 0):
            nulls += 1
        else:
            non_nulls += 1
        all += 1
    print("Analize Hist = " + str(nulls / all))
    if (nulls / all > 0.3):
        return 1
    else:
        return 0


class onePassViberate():
    CursorPositions = {
        "+ADD": (1785, 26),
        "Artist": (1794, 63),
        "ArtistName": (928, 503),
        "Country": (868, 563),
        "genre": (946, 599),
        "subgenre": (898, 621),
        "next_artist": (1057, 715),
        "spotify": (957, 395),
        "facebook": (926, 444),
        "twitter": (933, 515),
        "soundcloud": (936, 562),
        "youtube": (931, 606),
        "instagram": (921, 697),
        "next_links": (906, 736),
        "homepage": (938, 296),
        "google+": (984, 340),
        "itunes": (941, 527),
        "saveAndBack": (1038, 787),
        "add": (1064, 785),
        "exit": (1133, 373),
        "vib_logo": (70, 23)
    }

    def __init__(self, jsonObject, fpm, fp, sp):
        self.jsonObject = jsonObject

        self.getToAddArtist()

        self.inputArtistData(jsonObject["band_name"], jsonObject["country"])

        self.InputLinksFp(fpm, fp)

        self.InputLinksSp(sp)

        mouseMoveClickWait(self.CursorPositions["add"])
        time.sleep(5)

        im = ImageGrab.grab().crop((800 + relative_x, 400 + relative_y, 1115 + relative_x, 697 + relative_y))
        hist = ImageChops.difference(im, Image.open("SUCCESS.bmp")).histogram()
        print("for success analize hist")
        if analize_histograms(hist)==0:
            im_failed = ImageGrab.grab().crop((800,430,1120,650))
            hist_failed = ImageChops.difference(im_failed,Image.open("FAILED.bmp")).histogram()
            print("for fail analize hist")
            if analize_histograms(hist_failed) != 0:
                self.returnval = 2
            else:
                im_failed.save("failed\\FAILED_" + str(jsonObject["id"]) + ".bmp", "BMP")
                self.returnval = 0
        else:
            self.returnval = 1
        mouseMoveClickWait(self.CursorPositions["exit"])
        time.sleep(1)
        mouseMoveClickWait(self.CursorPositions["vib_logo"])

    def InputLinksFp(self, fpm, fp):
        for key in fpm.keys():
            mouseMoveClickWait(self.CursorPositions[key])
            keyboard.type(fpm[key])

        for key in fp.keys():
            mouseMoveClickWait(self.CursorPositions[key])
            keyboard.type(fp[key])

        mouseMoveClickWait(self.CursorPositions["next_links"])

    def InputLinksSp(self, sp):
        for key in sp.keys():
            mouseMoveClickWait(self.CursorPositions[key])
            keyboard.type(sp[key])

        mouseMoveClickWait(self.CursorPositions["saveAndBack"])

    def getToAddArtist(self):
        mouseMoveClickWait(self.CursorPositions["+ADD"])
        mouseMoveClickWait(self.CursorPositions["Artist"])
        mouseMoveClickWait(self.CursorPositions["ArtistName"])

    def inputArtistData(self, name, country):
        keyboard.type(name)
        mouseMoveClickWait(self.CursorPositions["Country"])
        keyboard.type(country)
        time.sleep(1)
        keyboardClick(Key.enter)
        time.sleep(1)
        mouseMoveClickWait(self.CursorPositions["genre"])
        keyboard.type("Rock / Metal")
        time.sleep(1)
        keyboardClick(Key.enter)
        time.sleep(1)
        mouseMoveClickWait(self.CursorPositions["subgenre"])
        keyboard.type("Metal")
        time.sleep(1)
        keyboardClick(Key.enter)
        time.sleep(0.5)
        mouseMoveClickWait(self.CursorPositions["next_artist"])


if __name__ == "__main__":
    import datetime, os
    import argparse
    from sys import executable
    from subprocess import Popen, CREATE_NEW_CONSOLE

    parser = argparse.ArgumentParser(description="Click some vib coins.")
    parser.add_argument('-n', default='20')
    parser.add_argument('-s', default='True')
    parser.add_argument('-d', default='30')
    arg = vars(parser.parse_args())

    n = int(arg['n'])
    ss = "True" == (arg['s'])
    startDelay = int(arg['d'])
    timesec = 50 * n + startDelay

    print("START this will take aroud "+str(timesec/60.0)+" minutes")

    with open('kdo_ne_obstaja.json', 'r') as fh:
        ary = json.loads(fh.read())

    with open("backups\\backup_" + str(datetime.datetime.now()).replace(" ", "_").replace(":", ".") + ".json",
              "w") as fh:
        fh.write(json.dumps(ary, indent=4))

    iteration = 0
    time.sleep(startDelay)
    print("really starting!")
    added = 0
    failed = 0
    bands_added = {}
    bands_failed ={}

    for band in ary:
        if ("added" not in band.keys()) or (band["added"] == 0):
            fpm, fp, sp, no1, no2 = linksToArrays(band["links"])
            if (no1 > 0 and no2 > 0):
                print(str(iteration) + ". adding " + band["band_name"] + " " + str(band["id"]))
                tmp = onePassViberate(band, fpm, fp, sp)

                if tmp.returnval == 1:
                    band["added"] = 1
                    band["time_added"] = str(datetime.datetime.now()).replace(" ", "_").replace(":", ".")
                    added += 1
                    bands_added[str(band["id"])] = band["band_name"]
                    print("Success!!")
                    iteration += 1

                elif tmp.returnval == 2:
                    print("Artist already added so fuck it")
                    failed += 1
                    bands_failed[str(band["id"])] = band["band_name"]
                    band["added"] = 1
                    iteration += 0

                else:
                    print("allright i failed misirably i should be ashamed of myself :((")
                    failed += 1
                    bands_failed[str(band["id"])] = band["band_name"]
                    band["added"] = 0
                    iteration += 0

                with open('kdo_ne_obstaja.json', 'w') as fh:
                    fh.write(json.dumps(ary, indent=4))
                if iteration >= n:
                    break

                if tmp.returnval == 1:
                    print("added going to sleep for 15s")
                    time.sleep(15)
                else:
                    print("added going to sleep for 5s")
                    time.sleep(5)
                keyboardClick(Key.f5)
                if tmp.returnval == 1:
                    print("added going to sleep for 15s")
                    time.sleep(15)
                else:
                    print("added going to sleep for 5s")
                    time.sleep(5)
            else:
                print("skipping " + band["band_name"] + " cause " + str(no1) + " " + str(no2))
        else:
            print("skipping " + band["band_name"] + " because added not in key" + str(
                ("added" not in band.keys())) + " or added" + str(band["added"]))

    if ss:
        print("starting crawler")
        Popen([executable, 'viberate-crawler.py'], creationflags=CREATE_NEW_CONSOLE)
    print("--------------------------------------")
    print("--------------------------------------")
    print("---------------SUMMARY----------------")
    print("Bands Added: "+str(added))
    print("Bands Failed: "+str(failed))
    print("--------------------------------------")
    print("Added bands:")
    print(bands_added)
    print("--------------------------------------")
    print("Failed bands:")
    print(bands_failed)
    print("--------------------------------------")
    if failed!=0:
        print("LOOK IN THE FAILED FOLDER AND RESOLVE")
    print("------------END-SUMMARY---------------")
    print("--------------------------------------")
