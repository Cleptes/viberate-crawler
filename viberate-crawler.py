# -*- coding: utf-8 -*-

import urllib3
import json
import time
import sys
import threading
from multiprocessing import Queue
urllib3.disable_warnings()
from get_urls import *

class endOfRequests(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def infinity():
    while True:
        yield

def getBandGenerator(q):
    print("bandgen")

    i=0

    with open('kdo_ne_obstaja.json','r') as fh:
        ary=json.loads(fh.read())
        i=ary[-1]['id']
    print("starting infinity")
    sys.stdout.flush()
    
    for _ in infinity():

        if q.qsize() > 30:
            print("WAITIN FOR Q TO EMPTY")
            sys.stdout.flush()
            time.sleep(1)
            continue

        http=urllib3.PoolManager()
        i+=1
        print("checking id "+str(i))
        sys.stdout.flush()
        
        r=http.request('GET', 'http://em.wemakesites.net/band/'+str(i)+'?api_key=e04dc436-0a10-4902-ae84-351f26ff86a4')
        
        if(r.status!=200):
            print("getting second time " + 'http://em.wemakesites.net/band/'+str(i)+'?api_key=e04dc436-0a10-4902-ae84-351f26ff86a4')
            r=http.request('GET', 'http://em.wemakesites.net/band/'+str(i)+'?api_key=e04dc436-0a10-4902-ae84-351f26ff86a4')
        js=json.loads(r.data)
        if 'band_name' in js["data"]:
            if(is_ascii(js["data"]['band_name']) and js["data"]["details"]["status"]=="Active"):
                dic={}
                dic["id"]=i
                dic['band_name']=(js["data"]['band_name'])
                #print(js["data"].keys())
                #sys.stdout.flush()
                dic['country']=(js["data"]["details"]['country of origin'])
                print("getting addresses for "+str(i))
                sys.stdout.flush()
                
                q.put(dic)
            #print ((js["data"]['band_name']).encode("utf-8"))
            #sys.stdout.flush()
            


def is_ascii(s):
    return all(ord(c) < 128 for c in s)



def checkVibrate(q):
    #with open('kdo_ne_obstaja.json', 'w') as the_file:
    #    the_file.write('[\n')
    print("start cckvibrate")
    http=urllib3.PoolManager()
    while 1:
        if(not q.empty()):
            data=q.get()
            name=data['band_name']
            name=name.replace(' ','-')#.encode("ascii","ignore")
            if(is_ascii(name)):
                pass
            else:
                print ("ni ascii" +name)
                sys.stdout.flush()
                continue
                
            
            #print("zacenam "+name)
            #sys.stdout.flush()
            print("checking name "+name)
            r=http.request('GET', 'https://www.viberate.com/artists/profile/'+name)
            print('https://www.viberate.com/artists/profile/'+name)
            #print("dubu req")
            sys.stdout.flush()
            if(r.status!=200):
                print("kr ni 200")
            body=r.data
            alije=body.find(b"404 <span>Not found</span>")
            #print("sm dubu")
            sys.stdout.flush()
            if(alije != -1):
                print("ne obstaja "+name)
                with open("kdo_ne_obstaja.json",'r') as fh:
                    bands=json.loads(fh.read())
                data['links'] = get_addresses(data['id'])
                print("got addresses for " + str(data['id']))
                with open('kdo_ne_obstaja.json', 'w') as fh:
                    data['url']='https://www.metal-archives.com/band/view/id/'+str(data['id'])
                    data['added']=0
                    bands.append(data)
                    fh.write(json.dumps(bands,indent=4))
                sys.stdout.flush()
            else:
                print("obstaja "+name)
                sys.stdout.flush()
            print(" ")
            sys.stdout.flush()

if __name__=="__main__":
    import datetime


    with open('kdo_ne_obstaja.json','r') as fh:
        ary=json.loads(fh.read())

    with open("backups\\backup_"+str(datetime.datetime.now()).replace(" ","_").replace(":",".")+".json","w") as fh:
        fh.write(json.dumps(ary,indent=4))

    q = Queue()
    thread2 = threading.Thread(target=checkVibrate,args=(q,))
    thread2.daemon=True

    thread1 = threading.Thread(target=getBandGenerator,args=(q,))
    thread1.daemon=True

    thread1.start()
    thread2.start()

    while 1:
        time.sleep(10000)
