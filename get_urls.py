# -*- coding: utf-8 -*-

import urllib3
import json
import time
import sys
import threading
from multiprocessing import Queue

urllib3.disable_warnings()


def getFullTagOf(txt, string):
    for line in txt.splitlines():
        if (line.lower().find(b">" + string + b"</a>") != -1):
            return line
    return ""


def getHrefFromLine(txt):
    for word in txt.split():
        if word.find(b"href") == 0:
            return (word[len("href='"):-1]).decode("utf-8")
    return ""


def checkIfValid(url):
    try:
        http = urllib3.PoolManager()
        if (url == ""):
            return ""
        r = http.request("GET", url, timeout=2)
        if r.status != 200:
            return ""
        return url
    except:
        return ""


def get_addresses(id):
    http = urllib3.PoolManager()
    r = http.request('GET', 'https://www.metal-archives.com/link/ajax-list/type/band/id/' + str(id))
    data = r.data
    links = {}
    anchors = [b'facebook', b'youtube', b'spotify', b'homepage', b'itunes', b'google+', b'instagram', b'soundcloud',
               b'twitter', b'bandcamp', b'official website', b'myspace']

    for anc in anchors:
        a = getHrefFromLine(getFullTagOf(data, anc))
        if a != "":
            print(a)

        links[anc.decode("utf-8")] = checkIfValid(a)
    if (links["spotify"].find("//play.spotify") != -1):
        links["spotify"] = ""

    return links


if __name__ == '__main__':
    '''
    with open('kdo_ne_obstaja.json','r') as fh:
        ary=json.loads(fh.read())
    
    for band in ary:
        if 'links' in band.keys():
            continue

        band['links']=get_addresses(band['id'])
        print (band)
        print(" ")
        with open('kdo_ne_obstaja.json','w') as fh:
            fh.write(json.dumps(ary,indent=4))
    '''
